/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.web.rest.concurrency.itest;

import static org.osgi.service.http.whiteboard.HttpWhiteboardConstants.HTTP_WHITEBOARD_SERVLET_PATTERN;

import java.lang.reflect.Field;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Random;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import javax.servlet.Servlet;
import javax.ws.rs.Path;

import org.amdatu.web.rest.jaxrs.ApplicationService;
import org.osgi.framework.Bundle;
import org.osgi.framework.BundleContext;
import org.osgi.framework.Constants;
import org.osgi.framework.FrameworkUtil;
import org.osgi.framework.ServiceReference;

import junit.framework.TestCase;

/**
 * Tests that starting and stopping bundles in random order causes the Wink services to work correctly. This tests AMDATUWEB-19.
 */
public class ConcurrencyTest extends TestCase {
    private static final String REST_PATH = "/rest";

    private final BundleContext m_context = FrameworkUtil.getBundle(getClass()).getBundleContext();
    private final Set<String> m_excluded = new HashSet<String>();

    public ConcurrencyTest() {
        m_excluded.add("junit.osgi");
        m_excluded.add("org.apache.felix.framework");
        m_excluded.add("org.apache.felix.dependencymanager");
        m_excluded.add("org.apache.felix.dependencymanager.shell");
        m_excluded.add("org.amdatu.web.rest.concurrency.itest.test");
        m_excluded.add("org.apache.felix.gogo.command");
        m_excluded.add("org.apache.felix.gogo.runtime");
        m_excluded.add("org.apache.felix.gogo.shell");
    }

    public void testConcurrentRegistrations() throws Exception {
        List<Bundle> bundles = new ArrayList<Bundle>();
        for (Bundle bundle : m_context.getBundles()) {
            if (!m_excluded.contains(bundle.getSymbolicName())) {
                bundles.add(bundle);
            }
        }

        System.out.println(">> Going to start/stop the following bundles:\n\t" + bundles);
        // These are the endpoint names that should always be available...
        String[] endpointNames = { "example1", "example2", "example3", "example4", "example5", //
            "example6", "example7", "example8", "example9", "app1", "app2" };

        Random prng = new Random();
        int iterations = 100;

        for (int i = 0; i < iterations; i++) {
            System.out.printf(">> Start run #%02d...%n", i);

            int successCount = 0;
            for (String endpointName : endpointNames) {
                if (testUrl(endpointName)) {
                    successCount++;
                }
            }

            if (endpointNames.length != successCount) {
                // Test will fail, output some debug info first
                printDebugInfo();
            }

            assertEquals("Failed to see all " + endpointNames.length + " endpoints!", endpointNames.length,
                successCount);

            System.out.printf(">> Stopping %d bundles...%n", bundles.size());
            Collections.shuffle(bundles, prng);

            for (Bundle b : bundles) {
                try {
                    b.stop();
                } catch (Exception exception) {
                    System.out.printf(">> Stopping bundle %s failed: %s.%n", b.getSymbolicName(),
                        exception.getMessage());
                }
            }

            System.out.printf(">> Starting %d bundles...%n", bundles.size());
            Collections.shuffle(bundles, prng);

            for (Bundle b : bundles) {
                try {
                    b.start();
                } catch (Exception exception) {
                    System.out.printf(">> Starting bundle %s failed: %s.%n", b.getSymbolicName(),
                        exception.getMessage());
                }
            }
        }
    }

    private void printDebugInfo() throws Exception {
        // Add some debug info
        try {
            System.out.println(" === DEBUG === ");
            Collection<ServiceReference<Servlet>> serviceReferences = m_context.getServiceReferences(Servlet.class, String.format("(%s=*)", HTTP_WHITEBOARD_SERVLET_PATTERN));
            System.out.println(serviceReferences.size() + " whiteboard servlets registered");
            for (ServiceReference<Servlet> serviceReference : serviceReferences) {
                Servlet servlet = m_context.getService(serviceReference);
                System.out.println(String.format("Servlet [pattern: '%s' class: '%s', service.id: '%s']",
                    serviceReference.getProperty(HTTP_WHITEBOARD_SERVLET_PATTERN),
                    servlet.getClass().getName(),
                    serviceReference.getProperty(Constants.SERVICE_ID)));

                if (servlet.getClass().getName().equals("org.amdatu.web.rest.wink.WinkServlet")) {
                    Field field = servlet.getClass().getDeclaredField("m_applicationService");
                    field.setAccessible(true);
                    ApplicationService applicationService = (ApplicationService) field.get(servlet);

                    for (Object o : applicationService.getSingletons()) {
                        Path pathAnnotation = o.getClass().getAnnotation(Path.class);
                        if (pathAnnotation != null) {
                            System.out.println(String.format("  Resource: [root: '%s', class: '%s']", pathAnnotation.value(), o.getClass().getName() ));
                        }
                    }
                    servlet = null;
                    m_context.ungetService(serviceReference);
                }
            }
        }catch (Exception e ) {
            System.out.println("Failed to dump debug info to sysout");
            e.printStackTrace();
            throw e;
        }
        System.out.println(" === DEBUG === ");
    }

    private boolean testUrl(String endpointName) throws Exception {
        URL url = new URL("http://localhost:8080" + REST_PATH + "/" + endpointName);

        int tries = 100, rc = 0;
        do {
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            try {
                rc = conn.getResponseCode();
            } finally {
                conn.disconnect();
            }
            if (rc != 200) {
                TimeUnit.MILLISECONDS.sleep(10);
            }
        } while (rc != 200 && tries-- > 0);
        return (rc == 200);
    }
}
