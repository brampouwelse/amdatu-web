/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.web.rest.wink;

import static org.amdatu.web.rest.jaxrs.AmdatuWebRestConstants.JAX_RS_APPLICATION_BASE;
import static org.amdatu.web.rest.jaxrs.AmdatuWebRestConstants.JAX_RS_RESOURCE_BASE;

import javax.servlet.Servlet;
import javax.ws.rs.core.Application;
import javax.ws.rs.ext.RuntimeDelegate;

import org.amdatu.web.rest.jaxrs.ApplicationService;
import org.amdatu.web.rest.jaxrs.JaxRsSpi;
import org.amdatu.web.rest.whiteboard.JaxRsWhiteboard;
import org.amdatu.web.rest.wink.util.DummyJaxRsSpi;
import org.amdatu.web.rest.wink.util.DummyRuntimeDelegate;
import org.apache.felix.dm.DependencyActivatorBase;
import org.apache.felix.dm.DependencyManager;
import org.apache.wink.common.internal.runtime.RuntimeDelegateImpl;
import org.osgi.framework.Bundle;
import org.osgi.framework.BundleContext;
import org.osgi.service.log.LogService;
import org.slf4j.impl.StaticLoggerBinder;

/**
 * This is the OSGi activator for the Amdatu REST framework based on Apache Wink.
 *
 * @author <a href="mailto:amdatu-developers@amdatu.org">Amdatu Project Team</a>
 */
public class Activator extends DependencyActivatorBase {

    @Override
    public void init(BundleContext context, DependencyManager manager) throws Exception {
        setRuntimeDelegate();
        // formatter:off
        manager
            .add(createComponent().setInterface(JaxRsSpi.class.getName(), null).setImplementation(DummyJaxRsSpi.class));

        manager.add(createComponent().setImplementation(StaticLoggerBinder.getSingleton())
            .add(createServiceDependency().setService(LogService.class).setRequired(false)));

        manager.add(createComponent().setImplementation(JaxRsWhiteboard.class)
            // Workaround for FELIX-5268: Service not unregistered while bundle is starting
            .add(createBundleDependency().setBundle(context.getBundle()).setStateMask(Bundle.ACTIVE)
                .setRequired(true))
            .add(createServiceDependency().setService(LogService.class).setRequired(false))
            .add(createServiceDependency()
                .setService(Application.class, withServiceProperty(JAX_RS_APPLICATION_BASE))
                .setCallbacks("applicationAdded", "applicationChanged", "applicationRemoved").setRequired(false))
            .add(createServiceDependency().setService(withServiceProperty(JAX_RS_RESOURCE_BASE))
                .setCallbacks("resourceAdded", "resourceChanged", "resourceRemoved").setRequired(false)));

        manager.add(createAdapterService(ApplicationService.class, null, "onAdd", "onChange", null)
            .setInterface(Servlet.class.getName(), null)
            .setImplementation(WinkServlet.class)
            .setCallbacks("dmInit", null, null, null)
            );
        // formatter:on
    }

    /**
     * Sets the runtime delegate, used to create instances of desired endpoint classes.
     */
    private void setRuntimeDelegate() {
        /*
         * OK, this is nasty, but necessary. Without this piece of code a NoClassDefFoundError is thrown
         * during servlet initialization on the class com.sun.ws.rs.ext.RuntimeDelegateImpl. The reason
         * for this is that JAX-RS by default delegates to this class if no other RuntimeDelegate
         * implementation could be found. Apache Wink does come with its own RuntimeDelegate, but during
         * initialization of this instance, RuntimeDelegate.getInstance() is invoked which causes a method
         * call to com.sun.ws.rs.ext.RuntimeDelegateImpl (see EntityTagMatchHeaderDelegate).
         * Other frameworks face similar issues using JAX-RS, see for example this URL for the exact same
         * problem in the Restlet framework: http://www.mail-archive.com/discuss@restlet.tigris.org/msg07539.html
         * The nasty fix is to set some dummy RuntimeDelegate first, then set the Wink RuntimeDelegateImpl
         */
        RuntimeDelegate.setInstance(new DummyRuntimeDelegate());
        RuntimeDelegate.setInstance(new RuntimeDelegateImpl());
    }

    private static String withServiceProperty(String property) {
        return "(" + property + "=*)";
    }

}
