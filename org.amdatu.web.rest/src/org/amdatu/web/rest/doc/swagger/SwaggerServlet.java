/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.web.rest.doc.swagger;

import static org.amdatu.web.rest.doc.swagger.Constants.HEADER_X_FORWARDED_PROTO;
import static org.amdatu.web.rest.doc.swagger.SwaggerUtil.documentOperations;
import static org.amdatu.web.rest.doc.swagger.SwaggerUtil.getDescription;
import static org.amdatu.web.rest.doc.swagger.SwaggerUtil.getPath;

import java.io.IOException;
import java.io.PrintWriter;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.SortedMap;
import java.util.TreeMap;
import java.util.concurrent.CopyOnWriteArrayList;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.Path;
import javax.ws.rs.core.MediaType;

import org.amdatu.web.rest.doc.Description;
import org.amdatu.web.rest.doc.NoDocumentation;
import org.amdatu.web.rest.doc.swagger.model.SwaggerAPI;
import org.amdatu.web.rest.doc.swagger.model.SwaggerAPIPath;
import org.amdatu.web.rest.doc.swagger.model.SwaggerModel;
import org.amdatu.web.rest.doc.swagger.model.SwaggerOperation;
import org.amdatu.web.rest.doc.swagger.model.SwaggerResource;
import org.amdatu.web.rest.doc.swagger.model.SwaggerResources;
import org.amdatu.web.rest.jaxrs.ApplicationService;
import org.apache.felix.dm.Component;
import org.osgi.framework.ServiceReference;

import com.google.gson.Gson;

/**
 * Listens to services in the framework and analyzes them for documentation. Also implements
 * the Servlet API so you can map this component to an endpoint and use it to show all
 * REST APIs that are available in the framework.
 */
@SuppressWarnings("serial")
public class SwaggerServlet extends HttpServlet {

    private static final JaxRsMethodComparator COMPARATOR = new JaxRsMethodComparator();

    private final CopyOnWriteArrayList<ApplicationService> m_restServices = new CopyOnWriteArrayList<ApplicationService>();

    public void addService(ServiceReference<ApplicationService> ref, ApplicationService service) {
        m_restServices.addIfAbsent(service);
    }

    public void removeService(ServiceReference<Object> ref, Object service) {
        m_restServices.remove(service);
    }

    protected SwaggerAPI createDocumentationFor(String baseURL, String rootPath, Class<?> clazz) {
        SwaggerModel models = new SwaggerModel();
        List<SwaggerAPIPath> apis = createDocumentationFor(models, rootPath, clazz);
    	return new SwaggerAPI(baseURL, rootPath, apis, models);
    }

    protected List<SwaggerAPIPath> createDocumentationFor(SwaggerModel models, String rootPath, Class<?> clazz) {
        // See section 3.3.1 of the JAX-RS specification v1.1...
        Method[] methods = clazz.getMethods();
        Arrays.sort(methods, COMPARATOR);

        List<SwaggerAPIPath> apis = new ArrayList<SwaggerAPIPath>();

        for (Method method : methods) {
            if (method.isAnnotationPresent(NoDocumentation.class)) {
                // Do not create documentation for this method...
                continue;
            }

            if (SwaggerUtil.isHttpMethod(method)) {
                List<SwaggerOperation> ops = documentOperations(models, method);
                if (!ops.isEmpty()) {
                    String path = getPath(method.getAnnotation(Path.class));
                    String doc = getDescription(method.getAnnotation(Description.class));

                    apis.add(new SwaggerAPIPath(rootPath.concat(path), doc, ops));
                }
            } else if (SwaggerUtil.isSubResourceLocator(method)) {
                String path = getPath(method.getAnnotation(Path.class));
                String subResourceRootPath = "/".equals(path) ? rootPath : rootPath.concat(path);
                List<SwaggerAPIPath> subApis = createDocumentationFor(models, subResourceRootPath, method.getReturnType());
                apis.addAll(subApis);
            }
        }
        return apis;
    }

    protected SwaggerResources createResourceListingFor(String baseURL, String rootPath, Map<String, Class<?>> services) {
        List<SwaggerResource> rs = new ArrayList<SwaggerResource>();
        for (Entry<String, Class<?>> entry : services.entrySet()) {
            Class<?> serviceType = entry.getValue();
            if (serviceType.isAnnotationPresent(Path.class)) {
                String path = getPath(entry.getKey());
                String doc = getDescription(serviceType.getAnnotation(Description.class));

                rs.add(new SwaggerResource(rootPath.concat(path), doc));
            }
        }
        return new SwaggerResources(baseURL, rs);
    }

    /**
     * Called by Felix DM when stopping this component.
     */
    protected final void dmStop(Component comp) {
        // Avoid unwanted leakage of resources...
        m_restServices.clear();
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String baseURL = getBaseURL(req);
        String requestPath = getPath(req.getPathInfo());

        SortedMap<String, Class<?>> services = getServices();
        if ("".equals(requestPath) || "/".equals(requestPath)) {
            SwaggerResources resources = createResourceListingFor(baseURL, req.getContextPath().concat(req.getServletPath()), services);

            writeAsJSON(resp, resources);
        } else {
            Class<?> serviceType = services.get(requestPath);
            if (serviceType != null) {
                SwaggerAPI apiDocs = createDocumentationFor(baseURL, requestPath, serviceType);

                writeAsJSON(resp, apiDocs);
            } else {
                // Swagger-UI b0rks when returning anything other than a valid JSON response,
                // so in case we didn't write anything, simply return an empty JSON string...
                writeAsJSON(resp, "");
            }
        }
    }

    private String getBaseURL(HttpServletRequest request) {
        int port = request.getServerPort();

        String protocol = request.getHeader(HEADER_X_FORWARDED_PROTO);
        if (protocol != null) {
            if ("https".equals(protocol)) {
                port = 443;
            }
        } else {
            protocol = request.isSecure() ? "https" : "http";
        }

        return protocol + "://" + request.getServerName() + ":" + port;
    }

    private SortedMap<String, Class<?>> getServices() {
        SortedMap<String, Class<?>> result = new TreeMap<>();
        for (ApplicationService applicationService : m_restServices) {
            String baseUri = getRestServletBaseUri(applicationService);
            for (Object service : applicationService.getSingletons()) {
                Class<?> serviceType = service.getClass();
                String path = getPath(serviceType.getAnnotation(Path.class));
                result.put(baseUri.concat(path), serviceType);
            }
        }
        return result;
    }

    private String getRestServletBaseUri(ApplicationService applicationService) {
        String winkContextUri = applicationService.getContextPath();
        if (winkContextUri == null) {
          winkContextUri = "";
        } if (winkContextUri.endsWith("/")) {
            winkContextUri = winkContextUri.substring(0, winkContextUri.length() -1);
        }
        winkContextUri = winkContextUri.concat(applicationService.getBaseUri());
        return winkContextUri;
    }

    private void writeAsJSON(HttpServletResponse resp, Object object) throws IOException {
        Gson gson = new Gson();
        resp.setContentType(MediaType.APPLICATION_JSON);
        PrintWriter writer = resp.getWriter();
        try {
            writer.append(gson.toJson(object));
        } finally {
            writer.flush();
            writer.close();
        }
    }

    private static class JaxRsMethodComparator implements Comparator<Method> {
        @Override
        public int compare(Method o1, Method o2) {
            Path pa1 = o1.getAnnotation(Path.class);
            String p1 = (pa1 != null) ? pa1.value() : "";

            Path pa2 = o2.getAnnotation(Path.class);
            String p2 = (pa2 != null) ? pa2.value() : "";

            int result = p1.compareTo(p2);
            if (result == 0) {
                result = o1.getName().compareTo(o2.getName());
            }

            return result;
        }
    }
}