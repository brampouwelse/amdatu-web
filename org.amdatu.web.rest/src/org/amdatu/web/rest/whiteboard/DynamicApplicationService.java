/**
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.web.rest.whiteboard;

import static org.amdatu.web.rest.jaxrs.AmdatuWebRestConstants.JAX_RS_APPLICATION_NAME;
import static org.amdatu.web.rest.jaxrs.AmdatuWebRestConstants.JAX_RS_RESOURCE_BASE;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Dictionary;
import java.util.Hashtable;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

import javax.ws.rs.Path;
import javax.ws.rs.ext.Provider;

import org.amdatu.web.rest.jaxrs.AmdatuWebRestConstants;
import org.amdatu.web.rest.jaxrs.ApplicationService;
import org.apache.felix.dm.Component;
import org.apache.felix.dm.DependencyManager;
import org.osgi.framework.ServiceReference;

/**
 * Implementation of the {@link ApplicationService} that adds additional JAX-RS resources and providers that are
 * registered with the {@link AmdatuWebRestConstants#JAX_RS_APPLICATION_NAME} service property matching the name of
 * this application and adds these to the list of singletons returned by the {@link #getSingletons()} method.
 *
 * <p>
 * This service can be used in a legacy mode to ease migration of rest services created for earlier versions of Amdatu Web (1.x / 2.x)
 * when enabled JAX-RS resources and providers that are registered without {@link AmdatuWebRestConstants#JAX_RS_APPLICATION_NAME} and
 * {@link AmdatuWebRestConstants#JAX_RS_RESOURCE_BASE} are also added to the list of singletons returned by the {@link #getSingletons()}
 * method.
 */
public class DynamicApplicationService extends StaticApplicationService {

    private final ConcurrentHashMap<ServiceReference<?>, Object> m_jaxRsResources;

    private final boolean m_legacyTrackingMode;

    private volatile Component m_component;

    public DynamicApplicationService(Object[] singletons, String base, String context, String name,
        boolean legacyTrackingMode) {
        super(singletons, base, context, name);
        m_legacyTrackingMode = legacyTrackingMode;
        m_jaxRsResources = new ConcurrentHashMap<>();
    }

    @Override
    public Object[] getSingletons() {
        List<Object> singletons = new ArrayList<>();

        Collections.addAll(singletons, super.getSingletons());
        singletons.addAll(m_jaxRsResources.values());

        return singletons.toArray();
    }

    protected final void init(Component component) {
        String filter = String.format("(%s=%s)", JAX_RS_APPLICATION_NAME, getName());
        if (m_legacyTrackingMode) {
            // When legacy tracking mode enabled also track services that don't have a JAX_RS_APPLICATION_NAME and JAX_RS_RESOURCE_BASE property
            String noApplicationIdAndResourceBasePresent =
                String.format("(!(|(%s=*)(%s=*)))", JAX_RS_APPLICATION_NAME, JAX_RS_RESOURCE_BASE);
            filter = String.format("(|%s%s)", noApplicationIdAndResourceBasePresent, filter);
        }

        // @formatter:off
        DependencyManager dm = component.getDependencyManager();
        component.add(dm.createServiceDependency()
            .setService(filter)
            .setRequired(false)
            .setCallbacks("onAdded", "onRemoved"));
        // @formatter:on
    }

    /**
     * Called by Felix DM for each service registration.
     */
    protected final void onAdded(ServiceReference<Object> serviceReference, Object service) {
        Class<? extends Object> type = service.getClass();

        if (type.isAnnotationPresent(Path.class) || type.isAnnotationPresent(Provider.class)) {
            m_jaxRsResources.putIfAbsent(serviceReference, service);
            updateServiceProperties();
        }
    }

    /**
     * Called by Felix DM for each service deregistration.
     */
    protected final void onRemoved(ServiceReference<Object> serviceReference, Object service) {
        if (m_jaxRsResources.remove(serviceReference, service)) {
            updateServiceProperties();
        }
    }

    private void updateServiceProperties() {
        Dictionary<Object,Object> properties = m_component.getServiceProperties();
        if (properties == null) {
            properties = new Hashtable<>();
        }

        properties.put("rootResourceEndpoints", getRootResourceEndpoints());
        m_component.setServiceProperties(properties);
    }

    private List<String> getRootResourceEndpoints() {
        return Arrays.stream(getSingletons()).filter(s -> s.getClass().isAnnotationPresent(Path.class))
            .map(s -> ((Path) s.getClass().getAnnotation(Path.class)).value()).collect(Collectors.toList());
    }

}
