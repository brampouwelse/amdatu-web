/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.web.rest.jaxrs;

import java.lang.reflect.Method;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * The RequestContext is used by the {@link JaxRsRequestInterceptor} to obtain request
 * specific information.
 */
public class RequestContext {

    private final Object m_resource;
    private final Method m_method;
    private final Object[] m_params;
    private final HttpServletRequest m_request;
    private final HttpServletResponse m_response;

    public RequestContext(Object resource, Method method, Object[] params, HttpServletRequest request,
        HttpServletResponse response) {
        m_resource = resource;
        m_method = method;
        m_params = params;
        m_request = request;
        m_response = response;
    }

    public Object getResource() {
        return m_resource;
    }

    public Method getMethod() {
        return m_method;
    }

    public Object[] getParams() {
        return m_params;
    }

    public HttpServletRequest getRequest() {
        return m_request;
    }

    public HttpServletResponse getResponse() {
        return m_response;
    }

}
