/**
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.web.rest.jaxrs;

import javax.servlet.ServletContext;

/**
 * Defines constants for the Amdatu web rest whiteboard services.
 */
public class AmdatuWebRestConstants {

    private AmdatuWebRestConstants() {
        // non-instantiable
    }

    /**
     * Service property specifying the base URI mapping for a JAX-RS application service.
     *
     * The value of this service property must be of type String, and will have a "/" prepended if no "/" exists.
     */
    public static final String JAX_RS_APPLICATION_BASE = "osgi.jaxrs.application.base";

    /**
     * Service property specifying the base URI mapping for a JAX-RS resource service.
     *
     * The value of this service property must be of type String, and will have a "/" prepended if no "/" exists.
     */
    public static final String JAX_RS_RESOURCE_BASE = "osgi.jaxrs.resource.base";

    /**
     * Service property specifying the name of a JAX-RS resource.
     *
     * Resource names should be unique among all resource service associated with a single Whiteboard implementation.
     *
     * The value of this service property must be of type String.
     */
    public static final String JAX_RS_RESOURCE_NAME = "osgi.jaxrs.name";

    /**
     * Service property specifying the name of the {@link ServletContext} that should be used for this JAX-RS application service
     *
     * The value of this service property must be of type String.
     */
    public static final String JAX_RS_APPLICATION_CONTEXT = "org.amdatu.web.rest.application.context";

    /**
     * Service property specifying the name for a JAX-RS application.
     *
     * The value of this service property must be of type String.
     */
    public static final String JAX_RS_APPLICATION_NAME = "org.amdatu.web.rest.application.name";

    /**
     * Service property specifying the type for a JAX-RS application.
     *
     * <p>
     * By default the static application type is used (see value {@link #JAX_RS_APPLICATION_TYPE_STATIC}).
     *
     * <p>
     * The value of this service property must be of type {@code String},
     * Allowed values are {@link #JAX_RS_APPLICATION_TYPE_STATIC}, {@link #JAX_RS_APPLICATION_TYPE_DYNAMIC},
     * {@link #JAX_RS_APPLICATION_TYPE_LEGACY}
     *
     */
    public static final String JAX_RS_APPLICATION_TYPE = "org.amdatu.web.rest.application.type";

    /**
     * Possible value for the {@link #JAX_RS_APPLICATION_TYPE} property indicating the application is a static application.
     *
     */
    public static final String JAX_RS_APPLICATION_TYPE_STATIC = "org.amdatu.web.rest.application.type.static";

    /**
     * Possible value for the {@link #JAX_RS_APPLICATION_TYPE} property indicating the application is a dynamic application.
     *
     * <p>
     * For a dynamic application additional resource and provider services with a matching application name property will be
     * added to the application
     */
    public static final String JAX_RS_APPLICATION_TYPE_DYNAMIC = "org.amdatu.web.rest.application.type.dynamic";

    /**
     * Possible value for the {@link #JAX_RS_APPLICATION_TYPE} property indicating the application is a legacy application
     *
     * <p>
     * For a legacy application additional resource and provider services will be tracked in a way that is compatible with the 2.x versions
     * of the org.amdatu.web.rest.wink implementation. Resource and provider services that don't have a {@link #JAX_RS_RESOURCE_BASE} or
     * {@link #JAX_RS_APPLICATION_NAME} service property specified AND resources with a matching {@link #JAX_RS_APPLICATION_NAME} will be
     * added to the application.
     */
    public static final String JAX_RS_APPLICATION_TYPE_LEGACY = "org.amdatu.web.rest.application.type.legacy";

    /**
     * Service property specifying the name or names of {@link JaxRsRequestInterceptor} instance(s) that should be used for an application.
     *
     * <p>
     * The value of this service property must be of type {@code String}, {@code String[]}, or {@code Collection<String>}.>.
     */
    public static final String JAX_RS_APPLICATION_REQUEST_INTERCEPTOR = "org.amdatu.web.rest.application.requestInterceptor";


}
