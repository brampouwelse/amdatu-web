/**
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.web.rest.jaxrs;

import javax.servlet.ServletContext;

/**
 * Service that represents a JAX-RS application
 */
public interface ApplicationService {

    /**
     * Get the name of the application
     *
     * @return the name of the application, can be null if no name is provided.
     */
    String getName();

    /**
     * Root resource and provider instances.
     *
     * @return array of resources and providers never null
     */
    Object[] getSingletons();

    /**
     * The base URI for this application
     *
     * @return the base URI never null
     */
    String getBaseUri();

    /**
     * The name of the {@link ServletContext} for this application
     *
     * @return name of the {@link ServletContext} never null
     */
    String getContextName();


    /**
     * The name of the {@link ServletContext} for this application
     *
     * @return name of the {@link ServletContext} never null
     */
    String getContextPath();

}
