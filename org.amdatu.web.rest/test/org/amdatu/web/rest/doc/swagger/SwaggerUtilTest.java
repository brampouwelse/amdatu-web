/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.web.rest.doc.swagger;

import static org.amdatu.web.rest.doc.swagger.SwaggerUtil.*;

import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.QueryParam;

import junit.framework.TestCase;

import org.amdatu.web.rest.doc.swagger.model.SwaggerModel;
import org.amdatu.web.rest.doc.swagger.model.SwaggerModelProperty;
import org.amdatu.web.rest.doc.swagger.model.SwaggerModelType;
import org.amdatu.web.rest.doc.swagger.model.SwaggerOperation;
import org.amdatu.web.rest.doc.swagger.model.SwaggerParameter;

/**
 * Test cases for {@link SwaggerUtil}.
 */
public class SwaggerUtilTest extends TestCase {

    /**
     * Tests that we can convert types with circular references correctly to a Swagger model.
     */
    public void testConvertCircularReferenceOk() {
        SwaggerModel models = new SwaggerModel();

        convertToSwaggerType(models, TreeNode.class);

        // Only TreeNode
        assertEquals(1, models.size());

        assertModelType(models, TreeNode.class.getName(), //
            "m_parent", TreeNode.class.getName(), null);
    }

    /**
     * Tests that we can convert various common types used in Java.
     */
    public void testConvertTypesOk() {
        SwaggerModel models = new SwaggerModel();

        convertToSwaggerType(models, TypeExamples.class);

        // Status & TypeExamples
        assertEquals(2, models.size());

        assertModelType(models, TypeExamples.class.getName(), //
            "m_primitiveInt", "integer", null, // int32
            "m_primitiveLong", "integer", null, // int64
            "m_primitiveInts", "array", "integer", // int32
            "m_ints", "array", "integer", // int32
            "m_set", "array", "number", // float
            "m_list", "array", "number", // number
            "m_map", "java.util.Map", null, //
            "m_status", "string", null);
    }
    
    //AMDATUWEB-46
    public void testConvertGenericTypesOk() {
        SwaggerModel models = new SwaggerModel();
        convertToSwaggerType(models, GenericTypeExample.class); 
        assertEquals(1, models.size());
    }

    /**
     * Tests the documentation of correctly annotated operations works as expected.
     */
    public void testDocumentGetStuffOperationOk() throws Exception {
        SwaggerModel models = new SwaggerModel();

        Method method = TypeExamples.class.getMethod("getStuff", Status.class, long.class, TreeNode.class);

        List<SwaggerOperation> doc = documentOperations(models, method);
        assertNotNull(doc);

        assertEquals(1, doc.size());
        SwaggerOperation op = doc.get(0);
        
        assertEquals("GET", op.method);
        assertEquals("getStuff", op.nickname);
        assertEquals(TypeExamples.class.getName(), op.type);
        
        List<SwaggerParameter> params = op.parameters;

        assertParameter(params.get(0), "status", "form", "string", "OK", "NOT_OK");
        assertParameter(params.get(1), "param1", "path", "integer"); // int64
        assertParameter(params.get(2), "TreeNode", "body", TreeNode.class.getName());
    }

    /**
     * Tests the documentation of correctly annotated operations works as expected.
     */
    public void testDocumentWaitMethodOk() throws Exception {
        SwaggerModel models = new SwaggerModel();

        Method method = TypeExamples.class.getMethod("wait");

        List<SwaggerOperation> doc = documentOperations(models, method);
        assertNotNull(doc);

        assertEquals(0, doc.size());
    }

    /**
     * Tests the documentation of method parameters works as expected.
     */
    public void testDocumentParametersOk() throws Exception {
        SwaggerModel models = new SwaggerModel();

        Method method = TypeExamples.class.getMethod("getStuff", Status.class, long.class, TreeNode.class);

        List<SwaggerParameter> doc = documentParameters(models, method);
        assertNotNull(doc);

        assertEquals(3, doc.size());

        assertParameter(doc.get(0), "status", "form", "string", "OK", "NOT_OK");
        assertParameter(doc.get(1), "param1", "path", "integer"); // int64
        assertParameter(doc.get(2), "TreeNode", "body", TreeNode.class.getName());
    }

    /**
     * AMDATUWEB-60
     */
    public void testQueryParamsNotRequired() throws Exception {
        SwaggerModel models = new SwaggerModel();

        Method method = TypeExamples.class.getMethod("withQueryParam", Status.class);

        List<SwaggerParameter> doc = documentParameters(models, method);
        assertNotNull(doc);

        assertEquals(1, doc.size());

        assertEquals(doc.get(0).required, Boolean.FALSE);
    }

    private static void assertModelType(SwaggerModel models, String typeName, String... propertyTriples) {
        SwaggerModelType modelType = models.get(typeName);
        assertNotNull("Type '" + typeName + "' not defined?!", modelType);

        int propCount = propertyTriples.length / 3;

        Map<String, SwaggerModelProperty> properties = modelType.properties;
        // A single field, m_parent, should be present...
        assertEquals("Unexpected amount of properties", propCount, properties.size());

        for (int i = 0; i < propertyTriples.length; i += 3) {
            String name = propertyTriples[i + 0];
            String type = propertyTriples[i + 1];
            String refType = propertyTriples[i + 2];

            SwaggerModelProperty prop = properties.get(name);
            assertNotNull("Property '" + name + "' not present?!", prop);
            assertEquals("Property '" + name + "' has incorrect type", type, prop.type);
            if (refType != null) {
                assertNotNull("Property '" + name + "' has no items?!", prop.items);
                assertEquals("Property '" + name + "' has incorrect refType", refType, prop.items.refType);
            } else {
                assertNull("Property '" + name + "' has items?!", prop.items);
            }
        }
    }

    private static void assertParameter(SwaggerParameter param, String name, String paramType, String dataType,
        String... enumValues) {
        assertNotNull(param);
        assertEquals(name, param.name);
        assertEquals(paramType, param.paramType);
        assertEquals(dataType, param.type);
        if (enumValues.length > 0) {
            assertEquals(Arrays.asList(enumValues), param.enumValues);
        } else {
            assertNull(param.enumValues);
        }
    }

    static enum Status {
        OK, NOT_OK;
    }

    @Path("/tree")
    static class TreeNode {
        private final TreeNode m_parent;

        public TreeNode() {
            this(null);
        }

        public TreeNode(TreeNode parent) {
            m_parent = parent;
        }

        @PUT
        public void doIt() {
            // Lazy bastard...
        }

        @GET
        @Path("/parent")
        public TreeNode getParent() {
            return m_parent;
        }
    }

    @Path("/types")
    @SuppressWarnings("unused")
    static class TypeExamples {
        private int m_primitiveInt;
        private long m_primitiveLong;

        private int[] m_primitiveInts;
        private Integer[] m_ints;
        private Set<Float> m_set;
        private List<? extends Number> m_list;
        private Map<String, Boolean> m_map;
        private Status m_status;

        @GET
        public TypeExamples getStuff(@FormParam("status") Status status, @PathParam("param1") long number, TreeNode node) {
            return this;
        }
        
        @GET
        public TypeExamples withQueryParam(@QueryParam("status") Status status) {
            return this;
        }
    }
    
    static class GenericTypeExample<T> {
        
        private List<T> list;
        
        public List<T> getList() {
            return list;
        }
    }
}
