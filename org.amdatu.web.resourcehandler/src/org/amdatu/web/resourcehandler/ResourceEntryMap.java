/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.web.resourcehandler;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * Represents a mapping of all resource entries parsed by {@link org.amdatu.web.resourcehandler.util.ResourceKeyParser}.
 */
public class ResourceEntryMap {
    private final Map<String, Map<String, ResourceEntry>> m_entries;

    public ResourceEntryMap() {
        m_entries = new HashMap<String, Map<String, ResourceEntry>>();
    }

    public void addEntry(String contextId, String alias, String path) {
        Map<String, ResourceEntry> entryMap = m_entries.get(contextId);
        if (entryMap == null) {
            entryMap = new LinkedHashMap<String, ResourceEntry>();
            m_entries.put(contextId, entryMap);
        }
        ResourceEntry entry = entryMap.get(alias);
        if (entry == null) {
            entry = new ResourceEntry(alias);
            entryMap.put(alias, entry);
        }
        entry.addPath(path);
    }

    public Collection<String> getContextIDs() {
        return m_entries.keySet();
    }

    public List<ResourceEntry> getEntry(String contextId) {
        Map<String, ResourceEntry> entryMap = m_entries.get(contextId);
        if (entryMap == null) {
            return Collections.emptyList();
        }
        return new ArrayList<ResourceEntry>(entryMap.values());
    }
}
