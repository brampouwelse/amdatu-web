/**
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.web.filters.redirect;

import java.io.IOException;
import java.util.Dictionary;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletResponse;

import org.osgi.service.cm.ConfigurationException;

public class RedirectFilter implements Filter {

    private static final String REDIRECT_TO = "redirectTo";

    private volatile String m_redirectTo;

    @Override
    public void doFilter(ServletRequest req, ServletResponse res, FilterChain chain)
        throws IOException, ServletException {
        HttpServletResponse response = (HttpServletResponse) res;
        response.sendRedirect(m_redirectTo);
    }

    protected final void updated(Dictionary<String, ?> properties) throws ConfigurationException {
        String redirectTo = (String) properties.get(REDIRECT_TO);

        if (redirectTo == null || redirectTo.trim().isEmpty()) {
            throw new ConfigurationException(REDIRECT_TO,
                "Required configuration property " + REDIRECT_TO + " missing");
        }
        m_redirectTo = redirectTo;
    }

    @Override
    public void init(FilterConfig arg0) throws ServletException {
        // do nothing
    }

    @Override
    public void destroy() {
        // do nothing
    }
}
