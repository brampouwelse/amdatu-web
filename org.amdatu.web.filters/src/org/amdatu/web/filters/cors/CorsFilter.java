/**
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.web.filters.cors;

import java.io.IOException;
import java.util.Dictionary;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletResponse;

import org.osgi.service.cm.ConfigurationException;
import org.osgi.service.cm.ManagedService;

public class CorsFilter implements Filter, ManagedService {

    public static final String ALLOW_CREDENTIALS = "allowCredentials";
    public static final String ALLOW_HEADERS = "allowHeaders";
    public static final String ALLOW_METHODS = "allowMethods";
    public static final String ALLOW_ORIGIN = "allowOrigin";
    public static final String EXPOSE_HEADERS = "exposeHeaders";

    private static final String DEFAULT_ALLOW_CREDENTIALS = "true";
    private static final String DEFAULT_ALLOW_HEADERS = "Origin, Content-Type, If-Match";
    private static final String DEFAULT_ALLOW_METHODS = "GET, POST, PUT, DELETE, OPTIONS, HEAD";
    private static final String DEFAULT_ALLOW_ORIGIN = "*";
    private static final String DEFAULT_EXPOSE_HEADERS = "Link, Location, ETag";

    private volatile String m_allowCredentials;
    private volatile String m_allowHeaders;
    private volatile String m_allowMethods;
    private volatile String m_allowOrigin;
    private volatile String m_exposeHeaders;

    @Override
    public void doFilter(ServletRequest req, ServletResponse res, FilterChain chain)
        throws IOException, ServletException {

        HttpServletResponse response = (HttpServletResponse) res;

        response.setHeader("Access-Control-Allow-Credentials", m_allowCredentials);
        response.addHeader("Access-Control-Allow-Headers", m_allowHeaders);
        response.setHeader("Access-Control-Allow-Methods", m_allowMethods);
        response.addHeader("Access-Control-Allow-Origin", m_allowOrigin);
        response.addHeader("Access-Control-Expose-Headers", m_exposeHeaders);

        chain.doFilter(req, response);
    }

    @Override
    public void updated(Dictionary<String, ?> properties) throws ConfigurationException {
        String allowCredentials = (String) properties.get(ALLOW_CREDENTIALS);
        m_allowCredentials = allowCredentials != null ? allowCredentials : DEFAULT_ALLOW_CREDENTIALS;

        String allowHeaders = (String) properties.get(ALLOW_HEADERS);
        m_allowHeaders = allowHeaders != null ? allowHeaders : DEFAULT_ALLOW_HEADERS;

        String allowMethods = (String) properties.get(ALLOW_METHODS);
        m_allowMethods = allowMethods != null ? allowMethods : DEFAULT_ALLOW_METHODS;

        String allowOrigin = (String) properties.get(ALLOW_ORIGIN);
        m_allowOrigin = allowOrigin != null ? allowOrigin : DEFAULT_ALLOW_ORIGIN;

        String exposeHeaders = (String) properties.get(EXPOSE_HEADERS);
        m_exposeHeaders = exposeHeaders != null ? exposeHeaders : DEFAULT_EXPOSE_HEADERS;
    }

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        // do nothing
    }

    @Override
    public void destroy() {
        // do nothing
    }

}
