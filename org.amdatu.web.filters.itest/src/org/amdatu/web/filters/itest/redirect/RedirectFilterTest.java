/**
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.web.filters.itest.redirect;

import static org.amdatu.testing.configurator.TestConfigurator.configure;
import static org.amdatu.testing.configurator.TestConfigurator.createComponent;
import static org.amdatu.testing.configurator.TestConfigurator.createServiceDependency;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.util.Dictionary;
import java.util.Hashtable;
import java.util.Properties;

import javax.servlet.Servlet;

import org.amdatu.testing.configurator.TestConfigurator;
import org.amdatu.web.filters.itest.TestServlet;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import org.osgi.framework.Constants;
import org.osgi.service.cm.ManagedServiceFactory;
import org.osgi.service.http.whiteboard.HttpWhiteboardConstants;

import com.github.kevinsawicki.http.HttpRequest;

@RunWith(JUnit4.class)
public class RedirectFilterTest {

    private static final String TEST_URL = "http://localhost:8080/redirect";

    private volatile ManagedServiceFactory m_filterFactory;

    @Before
    public void before() {
        Properties testServletProps = new Properties();
        testServletProps.put(HttpWhiteboardConstants.HTTP_WHITEBOARD_SERVLET_PATTERN, "/*");

        String managedServiceFactoryFilter = String.format("(%s=%s)", Constants.SERVICE_PID, "org.amdatu.web.filters.redirect");
        configure(this)
            .add(createComponent()
                    .setInterface(Servlet.class.getName(), testServletProps)
                    .setImplementation(new TestServlet()))
            .add(createServiceDependency()
                    .setService(ManagedServiceFactory.class, managedServiceFactoryFilter)
                    .setRequired(true))
            .apply();
    }

    @After
    public void after() {
        TestConfigurator.cleanUp(this);
    }

    @Test
    public void testRedirectFilter() throws Exception {
        Dictionary<String, Object> props = new Hashtable<>();
        props.put(HttpWhiteboardConstants.HTTP_WHITEBOARD_FILTER_REGEX, "/redirect/?");

        String target = "http://localhost:8080/target";
        props.put("redirectTo", target);;
        m_filterFactory.updated("test", props);
        assertHeaderValue(target, TEST_URL, "Location");

        m_filterFactory.deleted("test");
        assertHeaderValue(null, TEST_URL, "Location");
    }

    private void assertHeaderValue(String expected, String url, String header) throws IOException {
        assertEquals(expected, getHeader(url, header));
    }

    private String getHeader(String url, String header) throws IOException {
        HttpURLConnection connection = HttpRequest.get(url).getConnection();
        connection.setInstanceFollowRedirects(false);
        int responseCode = connection.getResponseCode();
        assertTrue(responseCode >= 200 && responseCode < 400);
        return connection.getHeaderField(header);
    }

}
