/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package bundle3httpcontext;

import java.io.IOException;
import java.net.URL;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.felix.dm.Component;
import org.osgi.framework.Bundle;
import org.osgi.service.http.HttpContext;

/**
 * 
 * Only used for testing!!
 * 
 */
public class TestHttpContext implements HttpContext {

    private volatile Bundle bundle;

    public void init(Component component) {
        bundle = component.getDependencyManager().getBundleContext().getBundle();
    }

    public boolean handleSecurity(HttpServletRequest request,
        HttpServletResponse response) throws IOException {

        return true;
    }

    public URL getResource(String name) {
        return bundle.getResource(name);
    }

    public String getMimeType(String name) {
        return null;
    }

}
