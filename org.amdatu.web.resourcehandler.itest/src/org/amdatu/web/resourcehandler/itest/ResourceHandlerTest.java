/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.web.resourcehandler.itest;

import java.io.IOException;
import java.util.List;
import java.util.Properties;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletResponse;

import org.osgi.framework.ServiceRegistration;

public abstract class ResourceHandlerTest extends ResourceHandlerTestBase {

    public void testBaseUrlsWithDefaultPageOk() throws Exception {
        assertEquals("default", getContents(URL_BUNDLE4_WITH_SLASH));
        assertEquals("default", getContents(URL_BUNDLE4_WITHOUT_SLASH));
    }

    public void testBaseUrlsWithoutDefaultPageOk() throws Exception {
        assertEquals(HTTP_NOT_FOUND, getResponseCode(URL_BUNDLE1_WITH_SLASH));
        assertEquals(HTTP_NOT_FOUND, getResponseCode(URL_BUNDLE1_WITHOUT_SLASH));
    }

    public void testDefaultCacheHeadersOk() throws Exception {
        String headerField = getHeader(URL_HELLO_WORLD, HDR_CACHE_CONTROL);
        assertEquals(DEFAULT_CACHE_CONTROL_VALUE, headerField);
    }

    public void testDefaultPageAtContextRootOk() throws Exception {
        assertEquals("default", getContents(URL_BUNDLE6_FULL_WITH_SLASH));
        assertEquals("default", getContents(URL_BUNDLE6_FULL_WITHOUT_SLASH));
        assertEquals("default", getContents(URL_BUNDLE6_WITHOUT_SLASH));
        assertEquals("default", getContents(URL_BUNDLE6_WITH_SLASH));
        assertEquals("default", getContents(URL_BUNDLE6_WITH_DEFAULT));
        assertEquals("x", getContents(URL_BUNDLE6_X));
    }

    public void testDefaultPageAtDirectoryRootOk() throws Exception {
        assertEquals("default", getContents(URL_BUNDLE4_ROOT_WITH_DEFAULT));
        assertEquals("default", getContents(URL_BUNDLE4_ROOT_WITHOUT_DEFAULT));
    }

    public void testDefaultPageAtSubDirOk() throws Exception {
        assertEquals("a", getContents(URL_BUNDLE4_A_WITH_DEFAULT));
        assertEquals("a", getContents(URL_BUNDLE4_A_WITHOUT_SLASH));
        assertEquals("a", getContents(URL_BUNDLE4_A_WITHOUT_DEFAULT));

        assertEquals("b_default", getContents(URL_BUNDLE4_B_WITH_DEFAULT));
        assertEquals("b_default", getContents(URL_BUNDLE4_B_WITHOUT_DEFAULT));

        assertEquals(HTTP_NOT_FOUND, getResponseCode(URL_BUNDLE4_B_NON_EXISTING));

        assertEquals("c", getContents(URL_BUNDLE4_C_C));
        assertEquals("default", getContents(URL_BUNDLE4_C_WITHOUT_SLASH));

        assertEquals(HTTP_NOT_FOUND, getResponseCode(URL_BUNDLE4_D_NON_EXISTING));
        assertEquals(HTTP_NOT_FOUND, getResponseCode(URL_BUNDLE4_D_WITHOUT_SLASH));
    }

    public void testDefaultPageOnlyForWebResourceV1dot1OrLater() throws Exception {
        // bundle 5 uses v1.0 of X-Web-Resource, hence its defined default pages should not be picked up...
        assertEquals(HTTP_NOT_FOUND, getResponseCode(URL_BUNDLE5_ROOT_WITHOUT_DEFAULT));
        // explicitly mentioned files should be picked up normally...
        assertEquals("default", getContents(URL_BUNDLE5_ROOT_WITH_DEFAULT));
    }

    public void testDeleteCacheHeaderConfigOk() throws Exception {
        Properties props = new Properties();
        props.put("cache.timeout", "10");

        String headerField;

        configureService(RESOURCE_HANDLER_PID, props);

        headerField = getHeader(URL_HELLO_WORLD, HDR_CACHE_CONTROL);
        assertEquals("max-age=10, must-revalidate", headerField);

        configureService(RESOURCE_HANDLER_PID, null);

        headerField = getHeader(URL_HELLO_WORLD, HDR_CACHE_CONTROL);
        assertEquals(DEFAULT_CACHE_CONTROL_VALUE, headerField);
    }

    public void testDisabledCacheHeadersOk() throws Exception {
        Properties props = new Properties();
        props.put("cache.timeout", -1L);

        configureService(RESOURCE_HANDLER_PID, props);

        String headerField = getHeader(URL_HELLO_WORLD, HDR_CACHE_CONTROL);
        assertNull(headerField);
    }

    /**
     * Test cases for AMDATUWEB-17.
     */
    public void testDoNotOverwriteExistingCacheControlHeaderOk() throws Exception {
        final String magicMarker = "magic marker";

        Filter cacheFilter = new Filter() {
            @Override
            public void destroy() {
                // Nop
            }

            @Override
            public void doFilter(ServletRequest req, ServletResponse resp, FilterChain chain) throws IOException,
                ServletException {
                ((HttpServletResponse) resp).addHeader(HDR_CACHE_CONTROL, magicMarker);
                chain.doFilter(req, resp);
            }

            @Override
            public void init(FilterConfig config) throws ServletException {
                // Nop
            }
        };

        Properties props = new Properties();
        props.put("pattern", "/.*");

        ServiceRegistration reg = m_context.registerService(Filter.class.getName(), cacheFilter, props);

        try {
            // Should only be one and only one Cache-Control header present...
            List<String> values = getHeaderValues(URL_HELLO_WORLD, HDR_CACHE_CONTROL);
            assertNotNull(values);
            assertEquals(1, values.size());
            assertEquals(magicMarker, values.get(0));
        } finally {
            reg.unregister();
        }

        // Should only still be one and only one Cache-Control header present...
        List<String> values = getHeaderValues(URL_HELLO_WORLD, HDR_CACHE_CONTROL);
        assertNotNull(values);
        assertEquals(1, values.size());
        assertEquals(DEFAULT_CACHE_CONTROL_VALUE, values.get(0));
    }

    public void testHelloWorld() throws Exception {
        assertEquals(HTTP_OK, getResponseCode(URL_HELLO_WORLD));
    }

    public void testMultiUrls() throws Exception {
        assertEquals(HTTP_OK, getResponseCode(URL_FAREWELL_WORLD));
        assertEquals(HTTP_OK, getResponseCode(URL_FAREWELL_AGAIN));
        assertEquals(HTTP_OK, getResponseCode(URL_FAREWELL_HELLO_WORLD));
    }

    public void testOwnContext() throws Exception {
        assertEquals(HTTP_OK, getResponseCode(URL_WOOT));
    }

    public void testRestartResource() throws Exception {
        assertEquals(HTTP_OK, getResponseCode(URL_HELLO_WORLD));
        assertEquals(HTTP_OK, getResponseCode(URL_WOOT));
        assertEquals(HTTP_OK, getResponseCode(URL_FAREWELL_WORLD));

        stopBundle(m_helloBundle);

        assertEquals(HTTP_NOT_FOUND, getResponseCode(URL_HELLO_WORLD));
        assertEquals(HTTP_OK, getResponseCode(URL_WOOT));
        assertEquals(HTTP_OK, getResponseCode(URL_FAREWELL_WORLD));

        startBundle(m_helloBundle);

        assertEquals(HTTP_OK, getResponseCode(URL_HELLO_WORLD));
        assertEquals(HTTP_OK, getResponseCode(URL_WOOT));
        assertEquals(HTTP_OK, getResponseCode(URL_FAREWELL_WORLD));
    }

    public void testRestartResourceHandler() throws Exception {
        assertEquals(HTTP_OK, getResponseCode(URL_WOOT));
        assertEquals(HTTP_OK, getResponseCode(URL_HELLO_WORLD));
        assertEquals(HTTP_OK, getResponseCode(URL_FAREWELL_WORLD));

        stopBundle(m_resourceHandler);

        assertEquals(HTTP_NOT_FOUND, getResponseCode(URL_WOOT));
        assertEquals(HTTP_NOT_FOUND, getResponseCode(URL_HELLO_WORLD));
        assertEquals(HTTP_NOT_FOUND, getResponseCode(URL_FAREWELL_WORLD));

        startBundle(m_resourceHandler);
        waitUntilResourcesAreAvailable();

        assertEquals(HTTP_OK, getResponseCode(URL_WOOT));
        assertEquals(HTTP_OK, getResponseCode(URL_HELLO_WORLD));
        assertEquals(HTTP_OK, getResponseCode(URL_FAREWELL_WORLD));
    }

    public void testStopOwnContext() throws Exception {
        assertEquals(HTTP_OK, getResponseCode(URL_WOOT));
        assertEquals(HTTP_OK, getResponseCode(URL_HELLO_WORLD));
        assertEquals(HTTP_OK, getResponseCode(URL_FAREWELL_WORLD));

        stopBundle(m_bundleOwnContext);

        assertEquals(HTTP_NOT_FOUND, getResponseCode(URL_WOOT));
        assertEquals(HTTP_OK, getResponseCode(URL_HELLO_WORLD));
        assertEquals(HTTP_OK, getResponseCode(URL_FAREWELL_WORLD));

        startBundle(m_bundleOwnContext);

        assertEquals(HTTP_OK, getResponseCode(URL_WOOT));
        assertEquals(HTTP_OK, getResponseCode(URL_HELLO_WORLD));
        assertEquals(HTTP_OK, getResponseCode(URL_FAREWELL_WORLD));
    }

    public void testUpdateCacheHeaderConfigOk() throws Exception {
        Properties props = new Properties();
        props.put("cache.timeout", "10");

        configureService(RESOURCE_HANDLER_PID, props);

        String headerField = getHeader(URL_HELLO_WORLD, HDR_CACHE_CONTROL);
        assertEquals("max-age=10, must-revalidate", headerField);
    }
}
