/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.web.rest;

import java.io.IOException;
import java.net.ConnectException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Dictionary;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Scanner;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.osgi.framework.BundleContext;
import org.osgi.framework.FrameworkUtil;
import org.osgi.framework.ServiceRegistration;

import junit.framework.TestCase;

/**
 * Base for registration tests
 */
public abstract class RegistrationTestBase extends TestCase {

    public static final int HTTP_OK = 200;
    public static final int HTTP_FORBIDDEN = 403;
    public static final int HTTP_NOT_FOUND = 404;
    public static final int HTTP_INTERNAL_SERVER_ERROR = 500;

    public static final String PREFIX = "http://localhost:8080";

    protected final BundleContext m_context = FrameworkUtil.getBundle(getClass()).getBundleContext();
    private final Set<ServiceRegistration<?>> m_srvRegs = new HashSet<>();

    public static Dictionary<String, Object> asDictionary(Object... props) {
        int size = props == null ? 0 : props.length / 2;
        Dictionary<String, Object> dict = new Hashtable<>(size);
        if (props != null) {
            for (int i = 0; i < props.length; i += 2) {
                dict.put(props[i].toString(), props[i + 1]);
            }
        }
        return dict;
    }

    public static void assertResponseCode(int expectedCode, String urlToRead) throws Exception {
        URL url = toURL(urlToRead);
        assertEquals(String.format("URL = %s", url.toExternalForm()), expectedCode, getResponseCode(url));
    }

    public static void assertResponseContent(String expectedContent, String urlToRead) throws Exception {
        URL url = toURL(urlToRead);
        assertEquals(String.format("URL = %s", url.toExternalForm()), expectedContent, getResponseContent(url));
    }

    public static String getResponseContent(String urlToRead) throws Exception {
        return getResponseContent(toURL(urlToRead));
    }

    public static URL toURL(String url) throws MalformedURLException {
        if (!url.startsWith("/") && !url.startsWith("http:")) {
            url = "/".concat(url);
        }
        if (!url.startsWith(PREFIX)) {
            url = PREFIX.concat(url);
        }
        return new URL(url);
    }

    private static int getResponseCode(URL url) throws IOException, InterruptedException {
        int rc = -1;
        int tries = 10;
        while (tries-- > 0 && rc < 0) {
            HttpURLConnection conn = null;

            try {
                conn = (HttpURLConnection) url.openConnection();
                rc = conn.getResponseCode();
            } catch (ConnectException e) {
                // Server not available yet, wait a little and try again...
                TimeUnit.MILLISECONDS.sleep(100L);
            } finally {
                if (conn != null) {
                    conn.disconnect();
                }
            }
        }
        return rc;
    }

    private static String getResponseContent(URL url) throws Exception {
        String content = null;
        int rc = -1;
        int tries = 10;
        while (tries-- > 0 && rc < 0) {
            HttpURLConnection conn = null;

            try {
                conn = (HttpURLConnection) url.openConnection();
                rc = conn.getResponseCode();

                // See <weblogs.java.net/blog/pat/archive/2004/10/stupid_scanner_1.html>
                try (Scanner scanner = new Scanner(rc < 400 ? conn.getInputStream() : conn.getErrorStream(), "UTF-8")) {
                    scanner.useDelimiter("\\A");

                    return scanner.hasNext() ? scanner.next() : null;
                }
            } catch (ConnectException e) {
                // Server not available yet, wait a little and try again...
                TimeUnit.MILLISECONDS.sleep(100L);
            } finally {
                if (conn != null) {
                    conn.disconnect();
                }
            }
        }
        return content;
    }

    protected final <T> ServiceRegistration<T> registerService(Class<T> type, T srv, Object... props) {
        ServiceRegistration<T> srvReg = m_context.registerService(type, srv, asDictionary(props));
        m_srvRegs.add(srvReg);
        return srvReg;
    }

    @Override
    protected final void tearDown() throws Exception {
        Iterator<ServiceRegistration<?>> iter = m_srvRegs.iterator();
        while (iter.hasNext()) {
            unregisterSilently(iter.next());
            iter.remove();
        }
    }

    protected final void unregisterSilently(ServiceRegistration<?> srvReg) {
        if (srvReg != null) {
            try {
                srvReg.unregister();
            } catch (IllegalStateException exception) {
                // Ignore, probably already unregistered...
            }
        }
    }
}
